# -*- coding: utf-8 -*-

import os


__name__ = 'py_versions'
__author__ = "Yordanis Tornes Medina"
__authoremail__ = "ytornes@gmail.com"
__url__ = ''

# __version__ = os.environ['GIT_TAG_VERSION'].lstrip('v') if 'GIT_TAG_VERSION' in os.environ else '0.0.2'
from pkg_resources import get_distribution, DistributionNotFound
try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    # package is not installed
    pass
