# -*- coding: utf-8 -*-

import os

from codecs import open
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'py_versions/__about__.py'), 'r') as f:
    exec (f.read(), about)

packages = find_packages(where='.')
# TODO: read requirements from requirements.txt instead requirements.txt read from here
requires = [
    'requests==2.21.0'
]

setup(
    name=about['__name__'],
    # version=about['__version__'],
    packages=packages,
    install_requires=requires,
    author=about['__author__'],
    author_email=about['__authoremail__'],
    url=about['__url__'],
    python_requires=">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, <3.8",
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    use_scm_version=True,
    setup_requires=['setuptools_scm==3.2.0']
)
